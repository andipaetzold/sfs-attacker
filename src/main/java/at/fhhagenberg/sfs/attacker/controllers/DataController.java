package at.fhhagenberg.sfs.attacker.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import at.fhhagenberg.sfs.attacker.domain.UserData;
import at.fhhagenberg.sfs.attacker.repositories.DataRepository;

@RestController
@RequestMapping("data")
@CrossOrigin
public class DataController {
    private final DataRepository repository;

    @Autowired
    public DataController(final DataRepository repository) {
        this.repository = repository;
    }

    @GetMapping
    public Iterable<UserData> findAll() {
        return repository.findAll();
    }

    @PostMapping
    public void create(final HttpServletRequest request,
                       @RequestHeader(value = "User-Agent") final String userAgent) {
        final UserData userdata = new UserData();

        userdata.setDate(new Date());
        userdata.setIp(request.getRemoteAddr());
        userdata.setUserAgent(userAgent);

        userdata.setHeaders(new ArrayList<String>());
        final Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            final String headerName = headerNames.nextElement();
            userdata.getHeaders().add(String.format("%s: %s", headerName, request.getHeader(headerName)));
        }

        repository.save(userdata);
    }
}
