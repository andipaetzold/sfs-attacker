package at.fhhagenberg.sfs.attacker.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;

import at.fhhagenberg.sfs.attacker.domain.UserData;

public interface DataRepository extends PagingAndSortingRepository<UserData, Long> {
}
